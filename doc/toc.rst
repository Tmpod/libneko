.. libneko documentation master file, created by
   sphinx-quickstart on Wed Jul  4 09:52:13 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to libneko's documentation!
===================================

**Current version**: |version|

**Current release**: |release|

A bunch of useful utilities, functions, and assorted bits and pieces for writing
bots with Discord.py's rewrite quickly and easily.

.. toctree::
   :maxdepth: 6
   :caption: Getting Started

   getting-started/pagination

.. toctree::
   :maxdepth: 6
   :caption: Technical Documentation

   technical/aggregates
   technical/asyncinit
   technical/attr-generator
   technical/checks
   technical/commands
   technical/converters
   technical/embeds
   technical/extras
   technical/filesystem
   technical/funcmods
   technical/logging
   technical/nekobot
   technical/other
   technical/pag
   technical/permissions
   technical/properties
   technical/singleton
   technical/strings
   technical/versioning

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
