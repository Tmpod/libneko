Pagination primer
=================

What is pagination? Why should I care?
--------------------------------------

This is a brief discussion of what this module is trying to achieve, and why. If you
cant stand walls of text, skip to the next heading to get to the good stuff: :ref:`the-gud-stuff`\.

There comes a time usually when you need to send a large amount of text to a user
over Discord. Unfortunately, Discord is overly crappy when it comes to messages
that are of any notable length, and even if you use embeds, you end up with a
very limited space for text (and embeds are generally garbage if they have more than a few
lines of text in them).

You can always chunk up your messages manually and send each chunk in order, but
that generally is seen as spam, and it never looks very professional.

Another issue is working out how to split your text up to begin with. You could
just do something like the following::

    def make_pages(text, page_length=2000):
        pages = []

        for i in range(0, len(text), page_length):
            pages.append(text[i:i+page_length])

        return pages


.. figure:: pagination1.png
   :scale: 100 %
   :align: center

   Eek. Spam.

...but now we have another issue. Your text is now being cut off mid word.

This still has the issue of spamming the chat with messages. It is kind of annoying
to look at, and difficult to read. You also lose any formatting, and it gets worse
if you try to wrap each message in code blocks! You may also want to limit how many
lines are shown per message.

You may say "well, what about :class:`discord.ext.commmands.Paginator`?"

While the implementation shipped with Discord.py's rewrite is an efficient and small paginator,
it does not handle text that is input as a single line at all.
It will not be able to split a very long single line up into multiple
chunks, and it still provides no neat and tidy way for displaying more than one page
at once without spamming the crap out of the chat. In addition, it is not very customisable,
and this quickly becomes clear when using it!

Wouldn't it be good if there was a way to handle formatting large chunks of text
correctly and neatly? Wouldn't it be great if there was some form of interactive
system on Discord that would let you show one page at a time, and be able to quickly
navigate between pages? This is what this submodule of libneko is designed for.

.. figure:: pagination2.png
   :scale: 100 %
   :align: center

   Pagination, in the way you want it.

A special paginator is implemented
that can create pages from any arbitrary input. The input does not have to be a string. Page
breaks can be added. You can limit or unlimit the number of lines to have per page. The
number of characters per page can be set. You can sanitise the input while generating the
pages (no more broken code blocks). You can append to the front of the pages instead of
the rear halfway through. You can even toggle allowing reformatting of text, so you can keep
fluent paragraphs where appropriate without having to hard-code in the line breaks, and then
keep code listings formatted in the same way they are input. Finally, it will not split
mid-word unless it has to. It will first try to find a more appropriate place to stop, such
as after a full stop, or on a space.


There is then a second set of classes called `Navigators.` These use Discord's reaction system
to add "buttons" to the bottom of a message. These buttons can be interacted with and programmed
to control the behaviour of the navigator.

.. figure:: pagination3.png
   :scale: 100%
   :align: center

   It even works on embeds!

Before we go any further, it is worth taking note of the meanings of the following definitions,
as you will quickly become confused otherwise. These are taken from the technical documentation.

I am aware that it is a bit wordy. I promise that this is the last wordy part though.


The ``pag`` submodule consists of four major components:

- The ``paginator``:
    this takes paragraphs, pages, and other bits of strings
    and outputs these bits in sized bins. In essence: bits of text go in: formatted
    pages that are within the specified character limit come out. This is the
    bread and butter behind how this submodule ensures your content will fit into
    a single Discord message. You can also pass multiple optional ``Substitutions``
    to this paginator to sanitise internal markdown formatting.

- The ``navigator``:
    a state machine that adds ``reactionbuttons`` to a message on
    Discord. These ``reactionbuttons`` are programmable and can allow the changing
    of the displayed page from a collection of pages produced by the ``paginator``
    or from custom-defined pages. Each button is added as a reaction by the navigator
    itself. You can then define who is allowed to trigger any of these reactions.

- The ``reactionbuttons``:
    an abstraction of what is essentially a Discord message
    reaction. These are defined with some custom logic (up to you what that logic
    actually is), and have an emoji. They get added to a ``navigator`` and that
    handles displaying them. When a user adds a reaction corresponding to an existing
    ``reactionbutton``, the ``navigator`` can remove that reaction and perform the
    logic you defined. That can be anything from changing a page, to kicking a user.
    You can do hopefully pretty much anything with this.

    There are also a bunch of predefined buttons in this module that you can use
    instead.

- The ``factory``:
    everyone hates making loads of objects repeatedly to do the same
    boring shit over and over again. Factories come to the rescue. Now, you can
    make a single object that behaves as a paginator, then run ``build`` or ``start``
    and boom: working ``navigator`` that is ready to use.

    Embed navigation factories are also now supported (experimentally). These are
    slightly more complicated, since Embed objects are much more customisable than
    markdown-formatted messages.

.. note::
    One should remain aware that a ``factory`` is, essentially a special implementation
    of ``paginator``. That means it will do everything that a ``paginator`` will do, to
    the same effect. The main difference is that to get meaningful output, you can call
    the built in ``start`` or ``build`` methods to get the whole thing working.

So, lets cut out the walls of text. They are a pain to write, and no one will read them
anyway. The rest of this is going to be working by example and showing what the output
looks like.

You are highly encouraged to try this out yourself and play with the system. I will try
to explain stuff briefly as I do it.

.. _the-gud-stuff:

Quick start
-----------

Sample data and getting set up
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

So, we are going to be using :download:`dummy-text.txt` in our examples from now on. I
will assume you have downloaded it, and put it in your current working directory with your
other files, and called it ``dummy-text.txt``.

I will then assume that you have at some point opened the file in Python, and read the
file contents into a variable that we can use later, called ``dummy_text``. I will also make
the assumption you have a basic bot working. If you are still new to this, then here is a little
framework to get you started. I am not going to teach how to use Discord.py's rewrite here, so
this is all you are going to get on it::

    #!/usr/bin/env python3.6
    # -*- coding: utf-8 -*-

    from discord.ext import commands
    from libneko import pag

    # We are not morons. We do not store our token in our repository. We put
    # it in a text file outside the repository (in this case, in the parent
    # directory, in "token.txt")
    with open('../token.txt') as fp:
        token = fp.read().strip()

    # Read the dummy text in.
    with open('dummy-text.txt') as fp:
        dummy_text = fp.read()

    bot = commands.Bot(command_prefix='lnt!')  # Lib Neko Test bot!

    @bot.command()
    async def test(ctx):
        """We will be coding in here in the next part."""

    bot.run(token)

Neat.


Factories
---------

First steps
~~~~~~~~~~~

We are going to introduce the factory classes first. These are your quick, dirty, simple-looking
classes that usually do everything you want if you are in a rush. I will assume you know Python,
and how to use Discord.py rewrite. If the following example scares you, I urge you learn to walk
before you can run. It will save both of us the frustration. Trust me.

So lets first try sending the text in the simplest way possible, remembering we are now working in
a command coroutine function (an ``async def`` with the ``@command`` decorator applied to it)::

    nav = pag.StringNavigatorFactory()

    # Insert the dummy text into our factory.
    nav += dummy_text

    nav.start(ctx)

That is it.

Surprised?

.. figure:: pagination4.png
   :scale: 100%
   :align: center

   Basic StringNavigator

This will make a big wall of text show up on Discord. Try changing the page with the buttons. It
all should work.

We might want only a small number of lines per page. That is simple enough::

    nav = pag.StringNavigatorFactory(max_lines=10)

    # Insert the dummy text into our factory.
    nav += dummy_text

    nav.start(ctx)

We can now run it again, and the pages should be shorter now.

.. figure:: pagination5.png
    :scale: 100%
    :align: center

    Same navigator, but now showing a max of 10 lines per page.

Notice how now there is room, the page number is shown at the top! If a message
is too close to the max size of a message, it will not show up, as to prevent errors
being raised by the Discord bot API.

.. important::
    The width of the Discord chat viewport depends on the device you are using, and
    a bunch of other factors as well (such as whether you have the window maximised, or have
    a different font size set, etc)

    This means that libneko cannot make assumptions about the length of a single line.

    For line-number counting to work, the content you are inserting MUST Have a physical
    newline ``\n`` after every line. If you do not do this, it will not be taken into account.

.. note::
    This line count is not a contract, it is a suggestion. If other factors affect the layout
    of the content, then you will find that the line count may be greater or less than what
    you expect. It is generally pretty accurate, though. Just remember that it will always ensure
    pages do not go over the character limit.

Embeds, instead
~~~~~~~~~~~~~~~

It is really simple to make this show in embeds instead. In fact, I am not going to bother
explaining this. You can see for yourself what changed::

    nav = pag.EmbedNavigatorFactory(max_lines=10)

    # Insert the dummy text into our factory.
    nav += dummy_text

    nav.start(ctx)

And behold.

.. figure:: pagination6.png
    :scale: 100%
    :align: center

    ¯\\_(ツ)_/¯

Boom. Simple. Can I go home now?

Customising your embed
~~~~~~~~~~~~~~~~~~~~~~

You may wish to customise how embeds are generated. This is a bit more tricky,
as embeds are fairly complex, and can do multiple things in multiple ways. The easiest way
for me to make it as usable as possible was to come up with the concept of an ``EmbedGenerator``.

An ``EmbedGenerator`` is simply an object that produces an embed. It is a decorated function that
takes three parameters. Essentially, it will be given the text to make a page for, and will be
expected to spit out an embed in the format you desire.

This is all a bit technical. If you didn't follow, don't worry. Hopefully the following example
should clear things up a bit for you.

So... suppose we want our embed to be red, and show a picture of Jeremy Clarkson in it, just because
why not? Top Gear is on the TV.

You will notice you have to specify the maximum characters to pass in the decorator. This is so that
the paginator knows in advance how much content to allocate each page. The reason for this is that
you may decide to put the content in a field instead of the description (as per the default). If you
do this, you will have a different maximum character limit, and it all gets kinda annoying.

.. important::
    Embeds have really pedantic limits. Go and study them at
    https://discordapp.com/developers/docs/resources/channel#embed-limits

Anyway, you can achieve this "amazing" new style with the following::

    clarkson_gif = 'https://bit.ly/2JImWP2'

    @pag.embed_generator(max_chars=2048)
    def cooler_embed(paginator, page, page_index):
        embed = discord.Embed(colour=0xFF0000, description=page)
        embed.set_image(url=clarkson_gif)
        return embed

    nav = pag.EmbedNavigatorFactory(factory=cooler_embed, max_lines=10)

    # Insert the dummy text into our factory.
    nav += bot.dummy_text

    nav.start(ctx)

Just remember that embeds still suck. They are a bit like children. They are obnoxious,
never seem to do what you want them to do, and behave seemingly randomly. They also make
you look bad when they do this. TLDR I don't like kids, and I don't like embeds.

.. figure:: pagination7.png
    :scale: 100%
    :align: center

    It is 2018 and Discord embeds still are fundementally broken.

.. note::
    If you want to provide your own custom page numbering, you can specify this in the
    decorator::

        @pag.embed_generator(max_chars=2048, provides_numbering=True)
        def cooler_embed(paginator, page, page_index):
            ...

    Notice the first embed example had a neat page number in the embed footer
    instead? The second one used the numbering system that is used in ``StringNavigator``\s by
    default. This is how you can change that.

The three parameters passed to the generator function are always the following:

1. A reference to the factory/paginator producing the pages.
2. The page that needs to be formatted.
3. The page index. This is zero based, so... for the first page, it will be 0, for the second, it
   will be 1, etc.

You can obtain the number of pages in total by calling ``len(paginator)``.

Options when creating factories
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

As you have seen, there are various options you can use with factories. A factory, as already
mentioned, is a specialisation of the :class:`libneko.pag.Paginator` class which handles formatting
your text correctly. For those of you who are not object-oriented wizards, this means that anything
that the ``Paginator`` can do will also be available for any of the ``*Factory`` classes.

You have already seen the ``max_lines`` argument::

    nav = pag.StringNavigatorFactory(max_lines=10)

    # Insert the dummy text into our factory.
    nav += dummy_text

    nav.start(ctx)

Other options you can specify include:
    max_chars:
        The max number of characters to allow per page. For ``StringNavigatorFactory`` objects, this
        defaults to 2000. For ``EmbedNavigatorFactory`` objects, it will default to the max length of
        the provided ``EmbedGenerator``. The default ``EmbedGenerator`` defaults to 2048.
    prefix:
        A string prefix to add to the start of each page.
    suffix:
        A string suffix to add to the end of each page.

Suffix and prefix are very useful if you need to display text in a code block. Both
default to an empty string ``''`` if unspecified.

Consider the following::

    nav = pag.StringNavigatorFactory(max_lines=10, prefix='```css', suffix='```')

    # Insert the dummy text into our factory.
    nav += dummy_text

    nav.start(ctx)

You should now get the following result.

.. figure:: pagination8.png
    :scale: 100%
    :align: center

    CSS was designed purely so that my Discord text can be yellow. Clearly.

.. note::
    If the prefix and suffix are non-empty, they will have a newline appended and
    prepended to them, respectively. This way, your code blocks don't immediately
    mess up if you forget to add a ``\n`` to them!

    If you did add a ``\n``, then don't worry. It will detect this, and leave it
    how it is. How helpful.

**To be continued...**

**...probably**