**This documentation is for**: |version| - |release|

.. image:: https://img.shields.io/badge/License-MIT-purple.svg
    :alt: PyPI - License
    
.. image:: https://img.shields.io/pypi/v/libneko.svg
    :alt: PyPI   
    :target: https://pypi.org/project/libneko/

.. image:: https://img.shields.io/badge/Lost%3F-%20rtfm!-pink.svg
    :alt: Docs
    :target: https://Tmpod.gitlab.io/libneko

.. image:: https://img.shields.io/discord/265828729970753537.svg
    :alt: Discord
    :target: https://discord.gg/GWdhBSp

.. image:: https://img.shields.io/pypi/implementation/libneko.svg
    :alt: PyPI - Implementation

.. image:: https://img.shields.io/badge/Python-3.6%203.7-yellow.svg

.. image:: https://gitlab.com/Tmpod/libneko/badges/master/pipeline.svg

.. image:: https://gitlab.com/Tmpod/libneko/badges/master/coverage.svg

.. image:: https://img.shields.io/badge/built%20with-libneko-ff69b4.svg
    :target: https://gitlab.com/Tmpod/libneko
    
.. image:: https://img.shields.io/badge/code%20style-black-000000.svg
    :target: https://github.com/ambv/black


Welcome to libneko's documentation!
===================================

A bunch of useful utilities, functions, and assorted bits and pieces for writing
bots with Discord.py's rewrite quickly and easily.


Table of Contents
~~~~~~~~~~~~~~~~~

.. toctree::
   :maxdepth: 6
   :caption: Getting Started

   getting-started/pagination

.. toctree::
   :maxdepth: 6
   :caption: Technical Documentation

   technical/aggregates
   technical/aiofiledb
   technical/aiojson
   technical/asyncinit
   technical/attr-generator
   technical/checks
   technical/clients
   technical/commands
   technical/converters
   technical/embeds
   technical/extras
   technical/filesystem
   technical/funcmods
   technical/logging
   technical/other
   technical/pag
   technical/permissions
   technical/properties
   technical/singleton
   technical/strings
   technical/versioning

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`


Installation
~~~~~~~~~~~~

To install libneko, you have two main options. The stable release (guaranteed to be
the most stable and "working" version, but not the most up to date),
or the most recent commit (likely broken or not working at all, use at your own risk!)

Stable version (recommended if you don't want shit breaking)::

    # Unices
    python3 -m pip install libneko
    
    # Windows (one of these)
    python -m pip install libneko
    py -m pip install libneko
    
Latest version (cant guarantee this even works)::

    # Unicies (with, and without git installed)
    python3 -m pip install git+https://gitlab.com/Tmpod/libneko
    python3 -m pip install https://gitlab.com/Tmpod/libneko/-/archive/master/libneko-master.zip
    
    # Windows (one of these should work. Windows is a pain in the arse. First two are with git, last two are without)
    python -m pip install git+https://gitlab.com/Tmpod/libneko
    py -m pip install git+https://gitlab.com/Tmpod/libneko
    python -m pip install https://gitlab.com/Tmpod/libneko/-/archive/master/libneko-master.zip
    py -m pip install https://gitlab.com/Tmpod/libneko/-/archive/master/libneko-master.zip

Quick start guides
~~~~~~~~~~~~~~~~~~

* `Pagination quickstart guide`_

.. _`Pagination quickstart guide`: getting-started/pagination.html

What does everything do?
~~~~~~~~~~~~~~~~~~~~~~~~

* aggregates_ - special lists, sets, queues, and proxies.
* aiofiledb_ - A poor man's asyncio database.
* aiojson_ - JSON file IO for asyncio.
* asyncinit_ - classes with awaitable constructors.
* attr_generator_ - key-value tags to stick on commands.
* checks_ - command checks.
* clients_ - ``discord.ext.commands.Bot`` and ``discord.ext.commands.AutoShardedBot`` but with more events and sanity checks.
* commands_ - more useful functions and features for commands and command groups.
* converters_ - converters for more operations, and redefinitions that your IDE/static type checker will not complain about.
* embeds_ - complete reimplementation of the Embed class with validation and verification checks to give meaningful error messages, and a cleaner default look.
* extras_ - premade cogs and extensions for your own sanity.
* filesystem_ - filesystem utilities and aiofiles' asyncio-friendly file implementation with type annotations.
* funcmods_ - extensions to the Python ``functools`` library module.
* logging_ - logging utilities.
* other_ - assorted random bits and pieces such as random colour generators.
* pagination_ - making bits of text into navigatable pages on Discord, and using reactions to manipulate the content being displayed.
* permissions_ - complete reimplementation of the permissions class to use a bit-field integer enumeration class.
* properties_ - cached properties!
* singleton_ - simple singleton metaclass for implementing singleton objects.
* strings_ - bits and bobs for working with strings.
* versioning_ - using Git to determine your bot's version.

.. _aggregates: technical/aggregates.html
.. _aiofiledb: technical/aiofiledb.html
.. _aiojson: technical/aiojson.html
.. _asyncinit: technical/asyncinit.html
.. _attr_generator: technical/attr_generator.html
.. _checks: technical/checks.html
.. _clients: technical/clients.html
.. _commands: technical/commands.html
.. _converters: technical/converters.html
.. _embeds: technical/embeds.html
.. _extras: technical/extras.html
.. _filesystem: technical/filesystem.html
.. _funcmods: technical/funcmods.html
.. _logging: technical/logging.html
.. _other: technical/other.html
.. _pagination: technical/pag.html
.. _permissions: technical/permissions.html
.. _properties: technical/properties.html
.. _singleton: technical/singleton.html
.. _strings: technical/strings.html
.. _versioning: technical/versioning.html

A sexy inheritance diagram
~~~~~~~~~~~~~~~~~~~~~~~~~~

The following explains how libneko, Python, discord.py, and all the other goodies 
all slot together to make up the stuff libneko works with. 

This might help show some of the complexity that all of the latter-mentioned abstract away from you.
There is a lot that makes this tick!

.. inheritance-diagram:: libneko.aggregates
                         libneko.asyncinit
                         libneko.aiojson
                         libneko.aiofiledb
                         libneko.attr_generator
                         libneko.checks
                         libneko.clients
                         libneko.commands
                         libneko.converters
                         libneko.embeds
                         libneko.extras
                         libneko.filesystem
                         libneko.funcmods
                         libneko.logging
                         libneko.other
                         libneko.pag
                         libneko.permissions
                         libneko.properties
                         libneko.singleton
                         libneko.strings
                         discord.abc
                         discord.activity
                         discord.audit_logs
                         discord.backoff
                         discord.calls
                         discord.channel
                         discord.client
                         discord.colour
                         discord.context_managers
                         discord.embeds
                         discord.emoji
                         discord.enums
                         discord.errors
                         discord.file
                         discord.gateway
                         discord.guild
                         discord.http
                         discord.invite
                         discord.iterators
                         discord.member
                         discord.message
                         discord.mixins
                         discord.object
                         discord.opus
                         discord.permissions
                         discord.player
                         discord.raw_models
                         discord.reaction
                         discord.relationship
                         discord.role
                         discord.shard
                         discord.state
                         discord.user
                         discord.utils
                         discord.voice_client
                         discord.webhook
                         discord.ext.commands
                         discord.ext.commands.bot
                         discord.ext.commands.context
                         discord.ext.commands.converter
                         discord.ext.commands.cooldowns
                         discord.ext.commands.core
                         discord.ext.commands.errors
                         discord.ext.commands.formatter
                         discord.ext.commands.view
                         functools
                         collections
                         typing
                         asyncio
                         websockets
                         aiohttp
                         requests
                         urllib.parse
                         random
                         os
                         sys
                         json
                         yaml

