``libneko.aiofiledb``
=====================

**A poor-man's asyncio-friendly key-value database that serializes to JSON on disk.**

.. inheritance-diagram:: libneko.aiofiledb

.. automodule:: libneko.aiofiledb
    :members:
    :inherited-members:
    :show-inheritance:

