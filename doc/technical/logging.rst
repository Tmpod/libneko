``libneko.logging``
===================

**Injecting loggers into your cogs!**

.. inheritance-diagram:: libneko.logging

.. automodule:: libneko.logging
    :members:
    :inherited-members:
    :show-inheritance:

