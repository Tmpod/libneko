``libneko.aggregates``
======================

**New collection wrappers! These include ordered sets, flippable dictionaries that can swap the keys and values to**
**allow bidirectional lookups, and a useful proxy class for dictionary types that allows the use of JavaScript-style**
**notation for accessing key-value pairs in a dict object.**

**Python's :mod:`collections` module is also exported here for convinience, and is documented below.**

.. inheritance-diagram:: libneko.aggregates

Libneko-specific classes
------------------------

.. automodule:: libneko.aggregates
    :members:
    :inherited-members:
    :show-inheritance:

Python ``collections``
----------------------

.. automodule:: collections
    :members:
    :inherited-members:
    :show-inheritance:
